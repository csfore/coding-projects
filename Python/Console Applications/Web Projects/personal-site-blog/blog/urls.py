from . import views
from django.urls import path
from django.views.generic import TemplateView

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html'), name='home'),
    path('projects/', TemplateView.as_view(template_name='projects.html'), name='projects'),
    path('qualifications/', TemplateView.as_view(template_name='qualifications.html'), name='qualifications'),
    path('info/', TemplateView.as_view(template_name='info.html'), name='info'),
    path("blog/", views.PostList.as_view(), name="blog"),
    path("<slug:slug>/", views.post_detail, name="post_detail"),
]
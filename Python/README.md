# Python Projects

These are apps written in Python, an interpreted language.

# Running

All you need to run these is Python >=3.6, which you can get [here](https://www.python.org/downloads/).
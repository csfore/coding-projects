# C++ Programs

These are my C++ programs, where I currently use 1 framework, and raw C++. The Qt applications are under it's respective directory, and console apps are also under it's respective directory.

# Building
Building C++ isn't too hard if you already know how to do it, if you use an IDE like CLion or Qt Creator, you can just run it, but if you wish to build it, you must have GCC or MingW installed, with either of those, you can run `g++ -o [name-you-want] [file].cpp`. Note: You must have those added to your environment variables to run without specifying G++'s location
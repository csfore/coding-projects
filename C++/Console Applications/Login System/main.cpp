#include <iostream>
#include <json/json.h>
#include <fstream>
#include "jsoncpp.cpp"

using namespace std;

int main() {
    ofstream filename;
    int userin;
    string userNameIn, passIn, firstNameIn;


    Json::Value firstNameJson, userNameJson, passJson, event;
    Json::Value user;
    Json::Value AllEvents(Json::arrayValue);


    Json::StreamWriterBuilder builder;
    builder["commentStyle"] = "None";
    builder["indentation"] = "   ";

    while(true) {
        // Simple menu to choose from
        cout << "1. LOGIN" << endl;
        cout << "2. CREATE" << endl;
        cout << "0. EXIT" << endl;

        cout << "Input: ";
        cin >> userin;

        if (userin == 1) {
            cout << "Username: ";
            cin >> userNameIn;
            cout << "Password: ";
            cin >> passIn;
            Json::Value root;
            std::ifstream test("test.json", std::ifstream::binary);

            Json::CharReaderBuilder builder;
            builder["collectComments"] = true;
            JSONCPP_STRING errs;
            if (!parseFromStream(builder, test, &root, &errs)) {
                std::cout << errs << std::endl;
                return EXIT_FAILURE;
            }
            std::cout << root[userNameIn] << std::endl;
            return EXIT_SUCCESS;

        } else if (userin == 2) {
            std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
            std::ofstream outputFileStream("test.json");
            cout << endl << "---Note: Please add users prior to exiting, as exiting and adding a user will overwrite the entire list.---" << endl;
            cout << "Input your first name ";
            cin >> firstNameIn;
            cout << "Input your username: ";
            cin >> userNameIn;
            cout << "Input your password: ";
            cin >> passIn;
            user["firstName"] = firstNameIn;
            user["password"] = passIn;

            event[userNameIn] = user;
            AllEvents.append(event);

            writer -> write(AllEvents, &outputFileStream);
            cout << "User created!" << endl;
        } else if (userin == 0){
            cout << "Exiting...";
            break;
        } else {
            cout << "Error: Invalid Input. Please try again." << endl;
        }
    }

    return 0;
}
#include "textfinder.h"
#include "ui_textfinder.h"
#include <QFile>
#include <QTextStream>

TextFinder::TextFinder(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::TextFinder)
{
    ui->setupUi(this);
}

TextFinder::~TextFinder()
{
    delete ui;
}


void TextFinder::on_findButton_clicked()
{
    QString searchString = ui->lineEdit->text();
    ui->textEdit->find(searchString, QTextDocument::FindWholeWords);
}

void TextFinder::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open the file");
    QFile file(fileName);
    currentFile = fileName;
    if(!file.open(QIODevice::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, "Warning", "Cannot open file:" + file.errorString());
        return;
    }
    setWindowTitle(fileName);
    QTextStream in(&file);
    QString text = in.readAll();
    ui->textEdit->setText(text);
    file.close();
}


void TextFinder::on_actionClose_triggered()
{
    QApplication::quit();
}


void TextFinder::on_actionUndo_triggered()
{
    ui->textEdit->undo();
}


void TextFinder::on_actionRedo_triggered()
{
    ui->textEdit->redo();
}


void TextFinder::on_actionCut_triggered()
{
    ui->textEdit->cut();
}


void TextFinder::on_actionCopy_triggered()
{
    ui->textEdit->copy();
}


void TextFinder::on_actionPaste_triggered()
{
    ui->textEdit->paste();
}


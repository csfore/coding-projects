# Coding Projects

Hi there, this is where I hope to store programming projects that I've worked on. I'll try to include build instructions where ever possible.

# Development Environment

I use GNU/Linux as a daily driver, so some code might not line-up with your operating system (I.E. Windows, MacOS, BSD-based systems) with certain libraries, like graphical ones, if so please open an issue and I can try to resolve it via virtualization.

## Navigation

Navigating this repository should be pretty simple, just follow the directory names.

## About Me

I'll keep this brief, because this is a repository about programs, but I'm a 17 year old graduate of Highland School of Technology, and I have a passion for computers. For more info, check out my site [here](https://chrisfore.us).